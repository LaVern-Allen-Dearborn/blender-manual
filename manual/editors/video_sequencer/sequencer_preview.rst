
*******************
Sequencer & Preview
*******************

This view type shows both the preview and the sequencer inside one editor.

.. figure:: /images/editors_vse_combi-sequencer-preview.png

   Figure 1: Combined Sequencer & Preview

In general, it's better to avoid this view type and instead have two editors,
one serving as the *Preview* and the other as the *Sequencer*.
Reasons for this include:

- Most of the *Preview* tools, such as Move and Rotate, are not available
  in *Sequencer & Preview*.
- You can't add a small editor (such as a File Browser) on the side that only
  takes up the height of the preview.
- You can't maximize the preview on another screen.

One way of getting two separate editors is to simply open the default
*Video Editing* :doc:`workspace </interface/window_system/workspaces>`.
(You may need to click the "+" icon to the right of the tabs to find it.)
