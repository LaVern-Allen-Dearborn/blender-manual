
#####################
  Write Curve Nodes
#####################

.. toctree::
   :maxdepth: 1

   set_curve_normal.rst
   set_curve_radius.rst
   set_curve_tilt.rst
   set_handle_positions.rst
   set_handle_type.rst
   set_spline_cyclic.rst
   set_spline_resolution.rst
   set_spline_type.rst
