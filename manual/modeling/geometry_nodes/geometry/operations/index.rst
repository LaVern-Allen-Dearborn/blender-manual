
############################
  Geometry Operation Nodes
############################

.. toctree::
   :maxdepth: 1

   bake.rst
   bounding_box.rst
   convex_hull.rst
   delete_geometry.rst
   duplicate_elements.rst
   merge_by_distance.rst
   split_to_instances.rst
   sort_elements.rst
   transform_geometry.rst

-----

.. toctree::
   :maxdepth: 1

   separate_components.rst
   separate_geometry.rst
